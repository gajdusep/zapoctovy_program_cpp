#include "KeyBoard.h"

sf::Vector2i KeyBoard::get_index_vector(const sf::Vector2f& position)
{
	auto size = purple_butt.getSize();
	float x_scale = float((xsize - 2 * margin_x) / button_per_x) / size.x;
	float y_scale = float((ysize - 2 * margin_y) / button_per_y) / size.y;
	size.x = (int)(size.x * x_scale); 
	size.y = (int)(size.y * y_scale);

	int index_x = int((position.x - start_position.x - margin_x) / size.x);
	int index_y = int((position.y - start_position.y - margin_y) / size.y);

	// std::cout << "index x: " << index_x << std::endl;
	// std::cout << "index y: " << index_y << std::endl;
	 
	int x = drawn_x_to_act_x(index_x);
	int y = drawn_y_to_act_y(index_y);

	sf::Vector2i vec = { x, y };
	return vec;
}

bool KeyBoard::check_margin(const sf::Vector2f& position)
{
	return (position.x <= start_position.x + margin_x ||
		position.x >= end_position.x - margin_x ||
		position.y <= start_position.y + margin_y ||
		position.y >= end_position.y - margin_y);
}

// rules for right clicking:
// 1) free space - normal filling
// 2) inside selected area already notes: delete them
// 3) this situation: aaaa    = already notes
//						bbbb  = selected area
//			- delete them
void KeyBoard::on_right_click(const sf::Vector2f& position, Controler& controler)
{
	if (!check_margin(position))
	{
		sf::Vector2i index_vector = get_index_vector(position);

		// check the corectness of right clicking
		switch (act_state)
		{
		case KeyBoard::right_clicked:
			// different note, forget it
			if (index_vector.y != last_clicked_index.y)
			{
				last_clicked_index = index_vector;
			}
			// the same note, different time, fill notes
			else
			{
				int min = std::min(index_vector.x, last_clicked_index.x);
				int max = std::max(index_vector.x, last_clicked_index.x);
				md->add_block(min, max, index_vector.y);
				act_state = not_clicked;
			}
			break;
		case KeyBoard::not_clicked:
			act_state = right_clicked;
			last_clicked_index = index_vector;
			break;
		default:
			break;
		}
		controler.play_tone(index_vector.y);
		
	}
}

// rules for left clicking:
// 1) if clicked already clicked -> empty note
// 2) if clicked empty -> full, short note
void KeyBoard::on_left_click(const sf::Vector2f& position, Controler& controler)
{
	if (check_margin(position))
	{
		decide_shift_click(position);
	}
	else
	{
		sf::Vector2i index_vector = get_index_vector(position);
		controler.play_tone(index_vector.y);
		md->add_single_note(index_vector.x, index_vector.y);
		act_state = not_clicked;
	}
}

void KeyBoard::decide_shift_click(const sf::Vector2f& position)
{
	if (position.x <= start_position.x + margin_x)
	{
		shift_left();
	}
	else if(position.x >= end_position.x - margin_x)
	{
		shift_right();
	}
	else if (position.y <= start_position.y + margin_y)
	{
		shift_up();
	}
	else if (position.y >= end_position.y - margin_y)
	{
		shift_down();
	}
}

void KeyBoard::shift_up()
{
	if (act_y_in_left_upper_corner - 1 >= 0)
	{
		act_y_in_left_upper_corner--;
	}
}

void KeyBoard::shift_down()
{
	if (act_y_in_left_upper_corner + button_per_y + 1 < int(md->midi_matrix[0].size()))
	{
		act_y_in_left_upper_corner++;
	}
}

void KeyBoard::shift_left()
{
	if (act_x_in_left_upper_corner - 1 >= 0)
	{
		act_x_in_left_upper_corner--;
	}
}

void KeyBoard::shift_right()
{
	if (act_x_in_left_upper_corner + button_per_x + 1 < int(md->midi_matrix.size()))
	{
		act_x_in_left_upper_corner++;
	}
}

KeyBoard::KeyBoard(std::shared_ptr<MidiData> mididata) : md(mididata)
{
	if (!grey_butt.loadFromFile("pictures/greybutt.png"))
		std::cout << "could not load file" << std::endl;
	if (!purple_butt.loadFromFile("pictures/purplebutt.png"))
		std::cout << "could not load file" << std::endl;
	if (!purple_butt_beg.loadFromFile("pictures/purplebutt_beg.png"))
		std::cout << "could not load file" << std::endl;
	if (!purple_butt_end.loadFromFile("pictures/purplebutt_end.png"))
		std::cout << "could not load file" << std::endl;
	if (!purple_butt_mid.loadFromFile("pictures/purplebutt_mid.png"))
		std::cout << "could not load file" << std::endl;
	if (!green_butt.loadFromFile("pictures/greenbutt.png"))
		std::cout << "could not load file" << std::endl;
	font.loadFromFile("pictures/font.ttf");
	load_note_texts("pictures/note_texts.txt");
	 
	button_per_x = 31;
	button_per_y = 20;
	act_x_in_left_upper_corner = 0;
	act_y_in_left_upper_corner = 30;
}

void KeyBoard::load_note_texts(const std::string& path)
{
	std::ifstream stream(path);
	std::string line;
	while (std::getline(stream,line))
	{
		note_texts.push_back(line);
	}
}

void KeyBoard::set_position(const sf::Vector2f& start, const sf::Vector2f& end)
{
	start_position = start;
	end_position = end;
	xsize = end_position.x - start_position.x;
	ysize = end_position.y - start_position.y;
}

void KeyBoard::DrawLinesKeyBoard(sf::RenderWindow& window)
{
	float sx = start_position.x;
	float sy = start_position.y; 
	float fx = end_position.x;
	float fy = end_position.y;

	float width = fx - sx;
	float height = fy - sy;
	
	float barwidth = margin_x;

	// bar for shifting in horizontal way
	sf::RectangleShape bary;
	bary.setFillColor(sf::Color::Cyan);
	bary.setSize(sf::Vector2f(barwidth, height - 2 * margin_y));
	bary.setPosition(sf::Vector2f(fx - barwidth, sy + margin_y));

	// bar for shifting in vertical way
	sf::RectangleShape barx;
	barx.setFillColor(sf::Color::Cyan);
	barx.setSize(sf::Vector2f(width - 2 * margin_x, barwidth));
	barx.setPosition(sf::Vector2f(sx + margin_x, fy - barwidth));

	// outer lines
	sf::RectangleShape board;
	float thickness = 5;
	board.setSize(sf::Vector2f(width, height));
	board.setPosition(sx, sy);
	board.setOutlineColor(sf::Color::Blue);
	board.setOutlineThickness(thickness);
	board.setFillColor(sf::Color::Transparent);


	window.draw(barx);
	window.draw(bary);
	window.draw(board);
}

void KeyBoard::drawKeyboard(sf::RenderWindow& window)
{
	DrawLinesKeyBoard(window);

	auto size = purple_butt.getSize();

	float x_scale = float((xsize - 2 * margin_x) / button_per_x) / size.x; 
	float y_scale = float((ysize - 2 * margin_y) / button_per_y) / size.y;
	size.x = (int)(size.x * x_scale); 
	size.y = (int)(size.y * y_scale);

	// bar showing notes
	for (int i = 0; i < button_per_y; i++)
	{
		sf::Text txt;
		txt.setFont(font);
		txt.setPosition(sf::Vector2f(start_position.x + 2,
			margin_y + start_position.y + i * size.y));
		txt.setCharacterSize(20);
		txt.setStyle(sf::Text::Bold);
		txt.setString(note_texts[act_y_in_left_upper_corner+i]);

		float x_scale_note =  size.x /( margin_x*2);
		sf::Sprite spr;
		spr.setTexture(green_butt);
		spr.setScale(x_scale_note, y_scale);
		spr.setPosition(sf::Vector2f(start_position.x,
			margin_y + start_position.y + i * size.y));
		
		window.draw(spr);
		window.draw(txt);
	}

	// bar showing number of tacts
	for (int i = 0; i < button_per_x; i++)
	{
		sf::Text txt;
		txt.setFont(font);
		txt.setPosition(sf::Vector2f(start_position.x + margin_x + i * size.x + 2,
			start_position.y));
		txt.setCharacterSize(20);
		txt.setStyle(sf::Text::Bold);
		txt.setString(std::to_string(i+act_x_in_left_upper_corner));

		sf::Sprite spr;
		spr.setTexture(green_butt);
		spr.setScale(x_scale, y_scale);
		spr.setPosition(sf::Vector2f(start_position.x + margin_x + i * size.x,
			start_position.y));

		window.draw(spr);
		window.draw(txt);
	}

	// actual keyboard
	for (int i = 0; i < button_per_x; i++)
	{
		for (int j = 0; j < button_per_y; j++)
		{
			sf::Sprite spr;
			int x = drawn_x_to_act_x(i);
			int y = drawn_y_to_act_y(j);
			
			// if not note on x,y
			if ((*md)[x][y][0] == 0)
			{
				spr.setTexture(grey_butt);
			}		
			// note on x,y
			else if ((*md)[x][y][0] == 1)
			{
				// if beginning of the note on x,y (either _beg or _butt)
				if ((*md)[x][y][1] == 1)
				{				
					// if beginning of the note on x+1,y
					if (x+1 >= (int)(md->midi_matrix.size()))
					{
						spr.setTexture(purple_butt);
					}
					else if (md->midi_matrix[x + 1][y][1] == 1 || 
						(*md)[x+1][y][0] == 0)
					{
						spr.setTexture(purple_butt);
					}
					else
					{
						spr.setTexture(purple_butt_beg);
					}					
				}
				// not beginning of the note on x,y
				else
				{
					// end
					if (x+1 >= (int)(md->midi_matrix.size()) ||
						(*md)[x+1][y][1] == 1 ||
						(*md)[x+1][y][0] == 0)
					{
						spr.setTexture(purple_butt_end);
					}
					// mid
					else
					{
						spr.setTexture(purple_butt_mid);
					}
				}
			}
			
			spr.setScale(x_scale, y_scale);
			spr.setPosition(sf::Vector2f(margin_x + start_position.x + i * size.x, 
					margin_y + start_position.y + j * size.y));
						
			window.draw(spr);
		}
	}
}
