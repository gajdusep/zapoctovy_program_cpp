#ifndef APP_H
#define APP_H

#include <SFML/Graphics.hpp>
#include "Viewer.h"
#include "Data.h"
#include "Controler.h"
#include <SFML\Audio\Music.hpp>
#include "Graphics_Parameters.h"
#include <iostream>

class App {
public:
	App();
	void run();
	void processEvents(Controler& controler);
	void render();
private:
	sf::RenderWindow window;
	std::shared_ptr<MidiData> mididata;
	Viewer viewer;
	Controler controler;
	Graphics_parameters gp;
};

#endif // !APP_H

