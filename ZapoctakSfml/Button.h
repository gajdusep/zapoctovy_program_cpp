#ifndef BUTTON_H
#define BUTTON_H

#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>

class Controler;
class Clickable
{
public:
	Clickable() {};
	bool check_clicked(const sf::Vector2f& position);
	virtual void draw(sf::RenderWindow& window) = 0;
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler) = 0;
	virtual void on_right_click(const sf::Vector2f& position, Controler& controler) = 0;
	sf::Vector2f start_position; // position of upper left corner of the button
	sf::Vector2f end_position; // position of downer right corner of the button
protected:
	bool clicked;
};

class ActionButton : public Clickable
{
public:
	virtual void load_texture_from_file(const std::string& path);
	void change_position(const sf::Vector2f& start, const sf::Vector2f& end);
	void draw(sf::RenderWindow& window);
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler) = 0;
	virtual void on_right_click(const sf::Vector2f& position, Controler& controler);
protected:
	sf::Texture play_texture;
	sf::Sprite spr;
};
 
class PlayButton : public ActionButton
{
public:
	PlayButton();
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler);
};


class StopButton : public ActionButton
{
public:
	StopButton();
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler);
};


class AddTactsButton : public ActionButton
{
public:
	AddTactsButton();
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler);
};

#endif // !BUTTON_H