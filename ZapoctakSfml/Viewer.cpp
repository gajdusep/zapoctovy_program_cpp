#include "Viewer.h"

void DrawBase(sf::RenderWindow& window)
{
	// get the size of window
	sf::View newview = window.getView();
	sf::Vector2f vc = newview.getSize();

	// rectangle on the sides of the window
	float thickness = 5;
	sf::ConvexShape convex;
	convex.setPointCount(4);
	convex.setPoint(0, sf::Vector2f(thickness, thickness));
	convex.setPoint(1, sf::Vector2f(thickness, vc.y - thickness));
	convex.setPoint(2, sf::Vector2f(vc.x - thickness, vc.y - thickness));
	convex.setPoint(3, sf::Vector2f(vc.x - thickness, thickness));
	convex.setOutlineColor(sf::Color::Blue);
	convex.setFillColor(sf::Color::Black);
	convex.setOutlineThickness(5);

	window.draw(convex);
}

void Viewer::Draw(sf::RenderWindow& window)
{
	DrawBase(window);
	for (auto&& x : vec_to_draw)
	{
		x->draw(window);
	}
}