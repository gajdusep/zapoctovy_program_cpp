#ifndef GRAPHICS_PARAMS_H
#define GRAPHICS_PARAMS_H

struct Graphics_parameters
{
public:
	Graphics_parameters()
	{
		window_size_x = 1400;
		window_size_y = 700;
		keyboard_start_x = 40;
		keyboard_start_y = 40;
		keyboard_end_x = window_size_x - keyboard_start_x;
		keyboard_end_y = window_size_y - 100;

		float y_start_coord = keyboard_end_y + 20;
		float y_end_coord = window_size_y - 20;
		float but_size_x = 100;
		// float but_size_y = 60;
		float but_space = 40;
		
		// y coordinates are the same for all buttons, "on the same line"
		play_start_y = y_start_coord;
		play_end_y = y_end_coord;
		pause_start_y = y_start_coord;
		pause_end_y = y_end_coord;
		stop_start_y = y_start_coord;
		stop_end_y = y_end_coord;
		addtacts_start_y = y_start_coord;
		addtacts_end_y = y_end_coord;

		play_start_x = keyboard_start_x;
		play_end_x = play_start_x + but_size_x;
		stop_start_x = play_end_x + but_space;
		stop_end_x = stop_start_x + but_size_x;
		pause_start_x = stop_end_x + but_space;
		pause_end_x = pause_start_x + but_size_x;
		addtacts_start_x = keyboard_end_x - but_size_x;
		addtacts_end_x = keyboard_end_x;
	}
	float window_size_x;
	float window_size_y;
	float keyboard_start_x;
	float keyboard_start_y;
	float keyboard_end_x;
	float keyboard_end_y;

	float play_start_x;
	float play_start_y;
	float play_end_x;
	float play_end_y;

	float pause_start_x;
	float pause_start_y;
	float pause_end_x;
	float pause_end_y;

	float stop_start_x;
	float stop_start_y;
	float stop_end_x;
	float stop_end_y;

	float addtacts_start_x;
	float addtacts_start_y;
	float addtacts_end_x;
	float addtacts_end_y;

};

#endif // !GRAPHICS_PARAMS.H

