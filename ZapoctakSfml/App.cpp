#include "App.h"

using namespace std;

App::App() : gp(), window(sf::VideoMode(1400, 700), "Midi piano", sf::Style::Titlebar | sf::Style::Close)
{

	// player class with new thread, start the thread
	std::unique_ptr<Player_MIDI> pm = std::make_unique<Player_MIDI>();
	pm->start();

	// make view
	sf::View view1;
	view1.reset(sf::FloatRect(0, 0, gp.window_size_x, gp.window_size_y));
	window.setView(view1);

	// make midi data
	mididata = std::make_shared<MidiData>();

	// make controler
	//Controler controler();
	controler.mididata = mididata;
	controler.pm = std::move(pm);

	// make keyboard
	std::shared_ptr<KeyBoard> keyboard = std::make_shared<KeyBoard>(mididata);
	keyboard->set_position(sf::Vector2f{ gp.keyboard_start_x, gp.keyboard_start_y },
		sf::Vector2f{ gp.keyboard_end_x, gp.keyboard_end_y });

	// make actionsbuttons: play, stop, plus_tacts
	
	std::shared_ptr<PlayButton> playbutton = std::make_shared<PlayButton>();
	playbutton->change_position(sf::Vector2f{ gp.play_start_x, gp.play_start_y }, 
		sf::Vector2f{ gp.play_end_x, gp.play_end_y });
	
	std::shared_ptr<StopButton> stopbutton = std::make_shared<StopButton>();
	stopbutton->change_position(sf::Vector2f{ gp.stop_start_x, gp.stop_start_y }, 
		sf::Vector2f{ gp.stop_end_x, gp.stop_end_y });
	
	std::shared_ptr<AddTactsButton> addtactsbutton = std::make_shared<AddTactsButton>();
	addtactsbutton->change_position(sf::Vector2f{ gp.addtacts_start_x, gp.addtacts_start_y },
		sf::Vector2f{ gp.addtacts_end_x, gp.addtacts_end_y });



	// add all possible buttons to cotroler
	controler.add_button(keyboard);
	controler.add_button(stopbutton);
	controler.add_button(playbutton);
	controler.add_button(addtactsbutton);

	// add all possible buttons to viewer
	viewer.add_button(keyboard);
	viewer.add_button(playbutton);
	viewer.add_button(stopbutton);
	viewer.add_button(addtactsbutton);
}

void App::run()
{
	// THE APPLICATION CYCLE
	while (window.isOpen())
	{
		processEvents(controler);
		render();		
	}
}

void App::render()
{
	window.clear();

	viewer.Draw(window);

	window.display();
}

void App::processEvents(Controler& controler)
{
	sf::Event event;
	
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed) window.close();
		controler.on_event(event);
	}
}