#ifndef KEYBOARD_H
#define KEYBOARD_H

#include <SFML/Graphics.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <vector>
#include <memory>
#include "Data.h"
#include "Button.h"
#include <stdio.h>
#include <iostream>
#include "Controler.h"
#include <fstream>

class KeyBoard : public Clickable 
{
public:
	KeyBoard(std::shared_ptr<MidiData> mididata);
	virtual void draw(sf::RenderWindow& window) { drawKeyboard(window); }

	virtual void on_right_click(const sf::Vector2f& position, Controler& controler);
	virtual void on_left_click(const sf::Vector2f& position, Controler& controler);

	bool check_margin(const sf::Vector2f& position);
	void set_position(const sf::Vector2f& start, const sf::Vector2f& end);
	void zoom(bool enlarge) {}

	/* ------ movement on the keyboard ------ */
	void shift_left();
	void shift_right();
	void shift_up();
	void shift_down();
	void decide_shift_click(const sf::Vector2f& position);

	void drawKeyboard(sf::RenderWindow& window);
	void DrawLinesKeyBoard(sf::RenderWindow& window);
private:
	/* ----- objects for viewing ------ */
	sf::Texture grey_butt;
	sf::Texture purple_butt;
	sf::Texture purple_butt_beg;
	sf::Texture purple_butt_end;
	sf::Texture purple_butt_mid;
	sf::Texture green_butt;
	sf::Font font;
	std::vector<std::string> note_texts;

	float margin_x = 40;
	float margin_y = 40;

	int button_per_x;
	int button_per_y;
	int act_x_in_left_upper_corner;
	int act_y_in_left_upper_corner;
	float xsize; // width of keyboard
	float ysize; // heigh of keyboard

	/*  -------------------------------- */

	// loads the txt file where are the names of notes (for example F#2) 
	void load_note_texts(const std::string& path); 
	
	// enum for right-clicking functionality
	enum Click_state { right_clicked, not_clicked }; 
	Click_state act_state = not_clicked;

	// for right-clicking functionality
	sf::Vector2i last_clicked_index; 
	
	std::shared_ptr<MidiData> md; 

	sf::Vector2i get_index_vector(const sf::Vector2f& position);

	/* ------- functions to correctly count the actual time and pitch ------- */
	int act_x_to_drawn_x(int x) { return x - act_x_in_left_upper_corner; };
	int act_y_to_drawn_y(int y) { return y - act_y_in_left_upper_corner; };
	int drawn_x_to_act_x(int x) { return x + act_x_in_left_upper_corner; };
	int drawn_y_to_act_y(int y) { return y + act_y_in_left_upper_corner; };
};

#endif // !KEYBOARD_H
