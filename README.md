# User documentation

## General

The purpose of this application is to let the user play with notes and easily change a note track that can be played. User can click on the keyboard, add new empty space (lengthen the track), play the track and stop it.

## Controls

### Clicking on music keyboard:

Left click: plays the tone. If the note is empty, a short note will appear. If there was already some note (short and long too), the note is canceled. After another click on the note, the short note will appear.

Right click: plays the tone. Enables user to make longer note. After first right click nothing will appear, after the second one (on the same pitch) the long note appears. If the longer note is set over already existing notes, all of them are canceled and only the long one remains.

### Moving the keyboard

The basic movement is provided by clicking on the right/left/upper/downer sides of the keyboard (the blue lines shift right and down, the green lines up and left). Mouse scrolling (or touchpad scroll - vertical and horizontal too) enables user to move faster on the keyboard.

### Length of track

By default 30 notes are displayed on the keyboard. If the length of track is too small, user can add 4 notes by clicking the button with plus sign on it. 

### Playing:

Play button plays the whole track, that was set by user by clicking on the music keyboard. Music can be stopped by clicking the stop button.

## Others

The window size is unchangable. 



# Code documenatation

## General

App is programmed in C++, the graphical interface with SFML library. The midi-playing functionality is taken from MIDIE class [available here](https://github.com/craigsapp/midifile). MIDIE class is slightly edited.
 
## App class

Main class of a whole program. After initialization (making all pointers, adding controler, viewer and all buttons) is executed the method run(). This method executes while cycle that repeats two functions: processEvents() and render().

`processEvents(Controler& c)` passes SFML event variable to controler that decides which action will be proceeded.

`render()` clears the `sf::RenderWindow window`, draws everything by calling `viewer`'s drawing methods and display it on the window. 

## Viewer class

Holds `vector<shared pointers<Clickable>> vec_to_draw`. 

Viewer has only two methods: `add_button(shared_ptr<Clickable> b)` that adds pointer to vector of buttons and method `Draw()`, that draws the desktop (shapes that don't have anything to do with buttons) and then goes throw the `vec_to_draw` and calls the draw method on every button.

## Data class

Holds three dimensional matrix of short. (Short and not bool is used for further extension, some other musical instrument could be added...) The first dimension is time, the second one pitch. The third dimension: [0] tells if note is playing there. [1] tells if the start of the note is set. This access is used because it enables the full information of the midi track and the length of notes.

Data class provides the `make_track(NoteTrack* track)` method that transfers matrix to NoteTrack implemented in MIDIE class.

Data class has a few methods for comfortable note-inserting and deleting, these methods are called from the Keyboard class. Namely: `delete_block_containing(...)`, `add_block(...)`, `add_single_note(...)`. 

The `midi_matrix` can be enlarged by `add_tacts()` method.

Data class declares the `operator[]` for simplier notation.

## Parameters struct

Holds parameters for visual structure of the user interface. It is separate from App class for better changeability of the visual parameters.


## MIDIE file 

Downloaded from [https://github.com/craigsapp/midifile](https://github.com/craigsapp/midifile).

These structs and classes are defined inside MIDIE file:
* `Note struct`: low level representation of note, holds pitch and volume
* `Line struct`
* `NoteTrack` class: used inside the Data class, used for making the playable track
* `MIDIE` class: only one method of this class is used in `Player_midi` class. This `PlayNoteTrack` method is edited. A `shared_ptr<int>& stopped` is passed. The stopped int says if the music was stopped or the MIDIE should continue with playing (0 means the music should stop playing, 1 means continue. Another int values reserved for another functionality.)

## Clickable class

Abstract class with `sf::Vector2f [start|end]_position` that determine the Clickable object size and position, `check_clicked(position)` method, that counts if the button was clicked, abstract methods `on_[left|right]_click(position, controler)` for left/right-click functionality.

## ActionButton class : Clickable

Abstract class that is a base class for `PlayButton`, `StopButton` and `AddTactsButton`. This kind of button holds `sf::Texture` and `sf::Sprite`. The method `on_right_click(position)` is not needed in this simple ActionButton class, thus only `on_left_click(position)` remains and it is still virtual.


## Keyboard class : Clickable

Inherits from Clickable. The most important and complex class - it determinates the behaviour of clicking, changes data, screens data etc.

### Keys clicking

Keyboard holds `shared_ptr<MidiData>`. After every left/right click the data are changed. The behaviour of clicking is described in the User documentation. 

Some helping methods are declared: `act_x_to_drawn_x(...)` etc. They contain calculation of coordinates and indeces in the data matrix.

### Other parts clicking

Actual midi track can be wide and there are about 80 notes, therefore only part of the keyboard can be viewed. Keyboard has shift methods, that changes shifts the keyboard left, right, up and down. These methods are called by clicking on the four sides of the keyboard. This can be changed to more elegant way, but this solution is enough for now and it enables easy movement on the keyboard.

An actual position is saved in `act_[x|y]_in_left_upper_corner`, number of keys to be displayed is saved in `button_per_[x|y]`. 


### Visualisation

Keys are visualised with `sf:Texture some_button_texture`. These textures differ in color (non clicked: `grey_butt`, clicked: `purple_butt`) and shape (different shape for the long note and different for the short ones). These textures are load during the initialization from the png files.

## Player_midi class

Owns a thread, `shared_ptr<MIDIE>`. 

From the controler is called the `play(std::unique_ptr<NoteTrack> track)` method. In `play(std::unique_ptr<NoteTrack> track)` method a thread is launched with the `Player_MIDI::play_midi_track(std::unique_ptr<NoteTrack> track)` method. This method calls `PlayNoteTrack(std::unique_ptr<NoteTrack> track, shared_ptr<int> stopped)` method from MIDIE. This method plays midi passed by `track`. 

If stop button is clicked, Player_midi class's method `stop()` is called, `stopped` pointer is changed to 0 value and `PlayNoteTrack(..)` stops with playing. 

Thanks to the thread, the clicking functionality is not endangered. If the user clicks the Keyboard during the playing, the `mididata` are changed, but it is not a problem, because it was already added to `NoteTrack` object. Therefore the track is finished with the original data.


## Controler class

Holds `vector<shared_ptr<Clickable>>` of all Clickable objects in the application.

Method `on_event(const sf::Event& event)` is the deciding function. It gets a raw sf::Event object and decides what will happen next. The application receive only a few type of events: `Event::MouseButtonPressed` and `Event::MouseWheelScrolled`. 

`MouseWheelScrolled` is event is important only when mouse is on the `Keyboard` object. In this case, the Keyboard methods for shifting are called.

`MouseButtonPressed` event implies that `on_clicked(const sf::Event& event, const sf::Vector2f& position)` method is called.

Method `on_clicked(const sf::Event& event, const sf::Vector2f& position)` goes through the vector of Clickable objects and if any of them was clicked, it calls their method `on_[left|right]_click(position)`. 

Methods `play_tone()`, `play_music()` and `stop_music()` are "wrapping" methods for music-playing functionality. In `play_music()` the NoteTrack is prepared from `mididata` and then `Player_MIDI`'s method is called. 



