#include "Data.h"

void MidiData::change_note(int time, int note)
{
	if (midi_matrix[time][note][0] == 0)
	{
		midi_matrix[time][note][0] = 1;
	}
	else
	{
		midi_matrix[time][note][0] = 0;
	}
}

void MidiData::setNote(int time, int note, int note_value)
{
	midi_matrix[time][note][0] = note_value;
}

void MidiData::set_beginning_of_note(int time, int note)
{
	midi_matrix[time][note][1] = 1;
}

void MidiData::add_tacts()
{
	for (int i = 0; i < size_of_tact; i++)
	{
		midi_matrix.push_back(std::vector<std::vector<short>>(pitch_range, { 0,0 }));
	}
}
 
void MidiData::add_block(int beg_time, int end_time, int note)
{
	for (int i = beg_time; i <= end_time; i++)
	{
		delete_block_containing(i, note);
	}

	midi_matrix[beg_time][note][1] = 1;
	for (int i = beg_time; i <= end_time; i++)
	{
		midi_matrix[i][note][0] = 1;
	}
}

void MidiData::delete_block_containing(int time, int note)
{
	if (midi_matrix[time][note][0] != 0)
	{
		int k = time;
		// get to beggining of long note
		while (midi_matrix[k][note][1] != 1)
		{
			k--;
		}
		midi_matrix[k][note][1] = 0;
		while (midi_matrix[k][note][1] != 1 &&
			midi_matrix[k][note][0] != 0)
		{
			midi_matrix[k][note][0] = 0;
			k++;
		}

	}
}

void MidiData::add_single_note(int time, int note)
{
	if (midi_matrix[time][note][0] != 0)
	{
		delete_block_containing(time, note);
	}
	else
	{
		midi_matrix[time][note][0] = 1;
		midi_matrix[time][note][1] = 1;
	}
}

std::unique_ptr<NoteTrack> MidiData::make_track()
{
	int vel = velocity;
	std::unique_ptr<NoteTrack> track = std::make_unique<NoteTrack>(0, 0);

	// make a track from midimatrix
	for (size_t i = 0; i < midi_matrix.size(); i++)
	{
		NoteTrack* track1 = new NoteTrack(0, 0);
		for (size_t j = 0; j < midi_matrix[0].size(); j++)
		{
			// check if some note should be ending
			if (i > 0 && j > 0)
			{
				// start of another note or maybe and of previous note
				if (midi_matrix[i][j][1] != 0 ||
					midi_matrix[i][j][0] == 0)
				{
					if (midi_matrix[i - 1][j][0] != 0)
					{
						track->Addline((short(i))*vel - 1, Note(108 - short(j), 0));
					}
				}
			}
		}
		for (size_t j = 0; j < midi_matrix[0].size(); j++)
		{
			// add beggining of a note
			if (midi_matrix[i][j][1] != 0)
			{
				track->Addline(short(i)*vel, Note(108 - short(j), 100));
			}
		}
	}
	track->End(1);
	return track;
}


MidiData::MidiData()
{
	for (int i = 0; i < beginning_time_size; i++)
	{
		midi_matrix.push_back(std::vector<std::vector<short>>(pitch_range, { 0,0 }));
	}
}