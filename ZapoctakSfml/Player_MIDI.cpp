#include "Player_MIDI.h"

void Player_MIDI::start()
{
	act_midie = std::make_unique<MIDIE>();
	stopped = std::make_shared<int>(0);
}

void Player_MIDI::play_midi_track(std::unique_ptr<NoteTrack> track)
{
	act_midie->PlayNoteTrack(std::move(track), stopped);
}

void Player_MIDI::play(std::unique_ptr<NoteTrack> track)
{
	if ((*stopped) == 0)
	{
		(*stopped) = 1;
		if (player_thread.joinable())
		{
			player_thread.join();
		}
		player_thread = std::thread(&Player_MIDI::play_midi_track, this, std::move(track));
		// std::cout << "stopped before it will be changed to 0: " << *stopped << std::endl;
		
	}
}

void Player_MIDI::stop()
{
	(*stopped) = 0;

	if (player_thread.joinable())
	{
		player_thread.join();
	}
}