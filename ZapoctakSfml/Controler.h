#ifndef CONTROLER_H
#define CONTROLER_H

#include <SFML/Graphics.hpp>
#include <vector>
#include <memory>
#include "Player_MIDI.h"
#include "Data.h"
#include <type_traits>

class Clickable;
typedef std::shared_ptr<Clickable> but_ptr;
class Controler
{
public:
	void on_event(const sf::Event& event);
	void on_clicked(const sf::Event& event, const sf::Vector2f& position);
	std::vector<but_ptr> buttons;
	void add_button(but_ptr but);
	void play_tone(int pitch);
	void play_music();
	void stop_music();
	void add_tacts();
	std::shared_ptr<MidiData> mididata;
	std::unique_ptr<Player_MIDI> pm;
};

#endif // !CONTROLER_H
