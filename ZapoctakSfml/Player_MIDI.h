#ifndef PLAYER_MIDI_H
#define PLAYER_MIDI_H

#include "MIDIE.h"
#include "Data.h"
#include <thread>
#include <stdio.h>

class Player_MIDI
{
public:
	Player_MIDI() : player_thread() {};
	~Player_MIDI() {
		(*stopped) = 0;
		if (player_thread.joinable())
		{
			player_thread.join();
		}
	}

	void start();
	void play_midi_track(std::unique_ptr<NoteTrack> track);
	void play(std::unique_ptr<NoteTrack> track);
	void stop();
private:
	std::shared_ptr<int> stopped;
	std::thread player_thread;
	std::unique_ptr<MIDIE> act_midie;
	enum state_of_player { playing, waiting};
	state_of_player act_state = waiting;
};

#endif // !PLAYER_MIDI_H