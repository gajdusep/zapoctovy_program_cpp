#include "Button.h"
#include "Controler.h"

bool Clickable::check_clicked(const sf::Vector2f& position)
{
	return (position.x > start_position.x && position.x < end_position.x &&
		position.y > start_position.y && position.y < end_position.y);
}

void ActionButton::change_position(const sf::Vector2f& start, const sf::Vector2f& end)
{
	start_position = start;
	end_position = end;
	auto size = play_texture.getSize();
	float x_scale = (end_position.x - start_position.x) / size.x;
	float y_scale = (end_position.y - start_position.y) / size.y;
	spr.setScale(x_scale, y_scale);
	spr.setPosition(start_position.x, start_position.y);
}

void ActionButton::draw(sf::RenderWindow& window)
{
	window.draw(spr);
}

void ActionButton::load_texture_from_file(const std::string& path)
{
	if (!play_texture.loadFromFile(path))
		std::cout << "could not load file" << std::endl;
}

void ActionButton::on_right_click(const sf::Vector2f& position, Controler& controler) { }

PlayButton::PlayButton()
{
	load_texture_from_file("pictures/play.png");
	spr.setTexture(play_texture);
}

void PlayButton::on_left_click(const sf::Vector2f& position, Controler& controler)
{
	controler.play_music();
}

StopButton::StopButton()
{
	load_texture_from_file("pictures/stop.png");
	spr.setTexture(play_texture);
}

void StopButton::on_left_click(const sf::Vector2f& position, Controler& controler)
{
	controler.stop_music();
}

AddTactsButton::AddTactsButton()
{
	load_texture_from_file("pictures/plus.png");
	spr.setTexture(play_texture);
}

void AddTactsButton::on_left_click(const sf::Vector2f& position, Controler& controler)
{
	controler.add_tacts();
}