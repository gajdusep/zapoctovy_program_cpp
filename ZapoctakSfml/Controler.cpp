#include "Controler.h"
#include "Button.h"
#include "KeyBoard.h"

void Controler::on_clicked(const sf::Event& event, const sf::Vector2f& position)
{
	for (size_t i = 0; i < buttons.size(); i++)
	{
		if (buttons[i]->check_clicked(position))
		{
			if (event.mouseButton.button == sf::Mouse::Left)
			{
				buttons[i]->on_left_click(position, *this);
			}
			if (event.mouseButton.button == sf::Mouse::Right)
			{
				buttons[i]->on_right_click(position, *this);
			}
		}
	}
}

void Controler::on_event(const sf::Event& event)
{
	if (event.type == sf::Event::MouseWheelScrolled)
	{
		// check if scrolling on keyboard
		sf::Vector2f scroll_pos = { (float)event.mouseWheelScroll.x, (float)event.mouseWheelScroll.y };	
		if (buttons[0]->check_clicked(scroll_pos))
		{
			if (event.mouseWheelScroll.wheel == sf::Mouse::VerticalWheel)
			{
				if (event.mouseWheelScroll.delta == 1)
				{
					std::static_pointer_cast<KeyBoard>(buttons[0])->shift_up();
				}
				else
				{
					std::static_pointer_cast<KeyBoard>(buttons[0])->shift_down();
				}
			}
			if (event.mouseWheelScroll.wheel == sf::Mouse::HorizontalWheel)
			{
				if (event.mouseWheelScroll.delta == 1)
				{
					std::static_pointer_cast<KeyBoard>(buttons[0])->shift_right();
				}
				else
				{
					std::static_pointer_cast<KeyBoard>(buttons[0])->shift_left();
				}
			}	
		}
	}
	if (event.type == sf::Event::MouseButtonPressed)
	{
		sf::Vector2f pos = { (float)event.mouseButton.x, (float)event.mouseButton.y };
		on_clicked(event, pos);
	}
}

void Controler::add_button(but_ptr but)
{
	buttons.push_back(but);
}

void Controler::play_tone(int pitch)
{
	std::unique_ptr<NoteTrack> track = std::make_unique<NoteTrack>(0, 0);
	track->Addline(1, Note(108 - pitch, 100));
	track->Addline(200, Note(108 - pitch, 0));
	track->End(1);
	pm->play(std::move(track));

}

void Controler::play_music()
{	
	auto track = mididata->make_track();
	pm->play(std::move(track));
}

void Controler::stop_music()
{
	pm->stop();
}

void Controler::add_tacts()
{
	mididata->add_tacts();

}