#ifndef DATA_H
#define DATA_H

#include <vector>
#include "MIDIE.h"

class MidiData
{
public:
	MidiData();
	std::vector<std::vector<std::vector<short>>> midi_matrix;

	// finds begining and ending of a note that contains certain note and deletes it
	void delete_block_containing(int time, int note);

	// adds long-lasting note
	void add_block(int beg_time, int end_time, int note);

	// adds single note
	void add_single_note(int time, int note);

	// make track to play
	std::unique_ptr<NoteTrack> make_track();

	int velocity = 200;

	void change_note(int time, int note);
	void setNote(int time, int note, int note_value);
	void set_beginning_of_note(int time, int note);
	void add_tacts();

	std::vector<std::vector<short>>& operator[] (int idx) { return midi_matrix[idx]; }
private:
	int beginning_time_size = 32;
	int pitch_range = 108 - 21;  // lowest note: A1 (21 in midi), highest C7 (108 in midi)
 	int size_of_tact = 4;
};

#endif