#ifndef VIEWER_H
#define VIEWER_H

#include "SFML\Graphics.hpp"
#include "KeyBoard.h"
#include <memory>

class Viewer
{
public:
	void add_button(std::shared_ptr<Clickable> button)
	{
		vec_to_draw.push_back(button);
	}
	void Draw(sf::RenderWindow& window);
private:	
	std::vector<std::shared_ptr<Clickable>> vec_to_draw;
};

#endif // !VIEWER_H
